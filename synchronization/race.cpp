#include <atomic>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

const int max_iterations = 1'000'000;

class Counter
{
    int value_ = 0;
    mutex mtx_value_;

public:
    Counter() = default;

    int value()
    {
        lock_guard<mutex> lk {mtx_value_};

        return value_;
    }

    Counter& operator++()
    {
        lock_guard<mutex> lk {mtx_value_}; // mtx_value_.lock()
        ++value_;
        // may_throw();

        return *this;
    } // mtx_value_.unlock()
};

Counter counter;

void worker()
{
    for (int i = 0; i < max_iterations; ++i)
    {
        ++counter;
    }
}

int main()
{
    auto start = chrono::high_resolution_clock::now();

    thread th1(worker);
    thread th2(worker);

    for (int i = 0; i < 10; ++i)
    {
        cout << "Counter = " << counter.value() << endl;
    }

    th1.join();
    th2.join();

    auto end = chrono::high_resolution_clock::now();
    float mseconds = chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << "Time: " << mseconds << " msec" << endl;

    cout << "Counter = " << counter.value() << endl;
}
