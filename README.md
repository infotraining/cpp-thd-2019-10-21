## Multithreading in C++

### Additonal information

#### Doc

* https://infotraining.bitbucket.io/cpp-thd/


#### login and password for VM:

```
dev  /  pwd
```

#### reinstall VBox addon (optional)

```
sudo /etc/init.d/vboxadd setup
```

#### proxy settings (optional)

We can add them to `.profile`

```
export http_proxy=http://aaa.bbb.ccc.ddd:port
export https_proxy=https://aaa.bbb.ccc.ddd:port
```

#### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

#### GIT

```
git clone https://infotraining@bitbucket.org/infotraining/cpp-thd-2019-10-21.git
```

#### Links

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

* [Codinghorror - Infinite space between words](https://blog.codinghorror.com/the-infinite-space-between-words/)

* [compiler explorer](https://gcc.godbolt.org/)

* http://stlab.cc/libraries/concurrency/


## Ankieta

* https://www.infotraining.pl/ankieta/cpp-thd-2019-10-21-kp
