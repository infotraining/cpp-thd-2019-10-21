#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "joining_thread.hpp"

using namespace std::literals;

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

class BackgroundWork
{
    const int id_;
    const std::string& text_;

public:
    BackgroundWork(int id, std::string text)
        : id_ {id}
        , text_ {std::move(text)}
    {
    }

    void operator()(std::chrono::milliseconds delay) const
    {
        std::cout << "BW#" << id_ << " has started..." << std::endl;

        for (const auto& c : text_)
        {
            std::cout << "BW#" << id_ << ": " << c << std::endl;

            std::this_thread::sleep_for(delay);
        }

        std::cout << "BW#" << id_ << " is finished..." << std::endl;
    }
};

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    std::thread thd0; // NOT-A-THREAD
    std::cout << thd0.get_id() << std::endl;

    const std::string text = "Concurrent";

    std::thread thd1(&background_work, 1, "Hello threads", 250ms);
    std::thread thd2(&background_work, 2, std::cref(text), 100ms);
    std::thread thd3(BackgroundWork {3, "function object"}, 150ms);
    std::thread thd4([] { background_work(4, "FROM LAMBDA", 75ms); });

    thd0 = std::move(thd1);

    thd0.join();
    thd2.join();
    thd3.detach();
    assert(!thd3.joinable());

    thd4.join();
    assert(!thd4.joinable());

    std::cout << "\n\n";

    const std::vector<int> source = {1, 2, 3, 4, 5, 6, 7};

    std::vector<int> target;
    std::vector<int> backup;

    std::thread thd_copy([&] { std::copy(begin(source), end(source), back_inserter(target)); });
    std::thread thd_backup([&] { std::copy(begin(source), end(source), back_inserter(backup)); });

    thd_copy.join();
    thd_backup.join();

    std::cout << "target: ";
    for (const auto& item : target)
        std::cout << item << " ";
    std::cout << "\n";

    std::cout << "backup: ";
    for (const auto& item : backup)
        std::cout << item << " ";
    std::cout << "\n";

    std::cout << "Main thread ends..." << std::endl;
}
