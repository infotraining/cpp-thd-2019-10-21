#include "joining_thread.hpp"
#include <atomic>
#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

class Service
{
public:
    virtual void run() = 0;
    virtual ~Service() = default;
};

class RealService : public Service
{
    std::string url_;
    std::mutex mtx_;

    // Service interface
public:
    RealService(const std::string& url)
        : url_ {url}
    {
        std::cout << "Creating a real service..." << std::endl;
        std::this_thread::sleep_for(1500ms);
        std::cout << "Openning connection to " << url_ << std::endl;
        std::this_thread::sleep_for(2s);
    }

    void run() override
    {
        std::lock_guard<std::mutex> lk(mtx_);
        std::cout << "RealService::run() - " << url_ << std::endl;
    }
};

namespace Atomics
{
    class LazyProxy : public Service
    {
        std::atomic<RealService*> real_service_ {};
        std::string url_;
        std::mutex mtx_;

    public:
        LazyProxy(const std::string& url)
            : real_service_ {nullptr}
            , url_ {url}
        {
        }

        LazyProxy(const LazyProxy&) = delete;
        LazyProxy& operator=(const LazyProxy&) = delete;

        ~LazyProxy()
        {
            delete real_service_;
        }

    public:
        void run() override
        {
            if (!real_service_.load())
            {
                std::lock_guard<std::mutex> lk {mtx_};

                if (!real_service_.load())
                {
                    RealService* temp = new RealService(url_);
                    ///////////////////////////////////////
                    real_service_.store(temp); // lazy init
                }
            }

            real_service_.load()->run();
        }
    };
}

class LazyProxy : public Service
{
    std::unique_ptr<RealService> real_service_;
    std::string url_;
    std::once_flag init_flag_ {};

public:
    LazyProxy(const std::string& url)
        : real_service_ {nullptr}
        , url_ {url}
    {
    }

    LazyProxy(const LazyProxy&) = delete;
    LazyProxy& operator=(const LazyProxy&) = delete;

public:
    void run() override
    {
        std::call_once(init_flag_, [this] { real_service_ = std::make_unique<RealService>(url_); });

        real_service_->run();
    }
};

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    LazyProxy service("http://google.com");

    {
        ext::joining_thread thd1([&service] {
            for (int i = 0; i < 10; ++i)
                service.run();
        });
        ext::joining_thread thd2([&service] { service.run(); });
    }

    std::cout << "Main thread ends..." << std::endl;
}
