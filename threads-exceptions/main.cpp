#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "joining_thread.hpp"

using namespace std::literals;

struct ThreadResult
{
    size_t value;
    std::exception_ptr eptr;
};

void background_work(size_t id, const std::string& text, ThreadResult& result)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(200ms);
    }

    try
    {
        result.value = static_cast<size_t>(text.at(3));
    }
    catch (...)
    {
        std::cout << "Exception has been caught...\n";
        result.eptr = std::current_exception();
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    ThreadResult result;

    {
        ext::joining_thread thd1 {&background_work, 42, "abc", std::ref(result)};
    } // join

    if (result.eptr)
    {
        try
        {
            std::rethrow_exception(result.eptr);
        }
        catch (const std::out_of_range& e)
        {
            std::cout << "Catching an exception in main..." << e.what() << "\n";
        }
    }
    else
    {
        std::cout << "Result: " << result.value << "\n";
    }

    std::vector<ThreadResult> results(4);
    {
        std::vector<ext::joining_thread> thds(4);

        for (size_t i = 0; i < 4; ++i)
            thds[i] = ext::joining_thread(&background_work, i + 1, "xt", std::ref(results[i]));
    }

    for (auto& r : results)
    {
        if (r.eptr)
        {
            try
            {
                std::rethrow_exception(r.eptr);
            }
            catch (const std::out_of_range& e)
            {
                std::cout << "Caught an exception..." << e.what() << "\n";
            }
        }
        else
        {
            std::cout << "Result: " << r.value << "\n";
        }
    }

    std::cout << "Main thread ends..." << std::endl;

    std::cout << "HC: " << std::thread::hardware_concurrency() << "\n";
}
