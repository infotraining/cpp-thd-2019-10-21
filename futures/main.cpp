#include "joining_thread.hpp"
#include <cassert>
#include <chrono>
#include <functional>
#include <future>
#include <iostream>
#include <random>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

int calculate_square(int x)
{
    std::cout << "Starting calculation for " << x << " in " << std::this_thread::get_id() << std::endl;

    std::random_device rd;
    std::uniform_int_distribution<> distr(100, 5000);

    std::this_thread::sleep_for(std::chrono::milliseconds(distr(rd)));

    if (x % 3 == 0)
        throw std::runtime_error("Error#3");

    return x * x;
}

void save_to_file(const std::string& filename)
{
    std::cout << "Saving to file: " << filename << std::endl;

    std::this_thread::sleep_for(3s);

    std::cout << "File saved: " << filename << std::endl;
}

void futures_with_packaged_tasks()
{
    // 1-st way - packaged_task
    std::packaged_task<int()> pt1([] { return calculate_square(8); });
    std::packaged_task<int(int)> pt2(&calculate_square);
    std::packaged_task<void()> pt3 {[] { save_to_file("file.dat"); }};

    std::future<int> f1 = pt1.get_future();
    std::future<int> f2 = pt2.get_future();
    std::future<void> f3 = pt3.get_future();

    std::thread thd1 {std::move(pt1)};
    ext::joining_thread thd2 {std::move(pt2), 9};
    ext::joining_thread thd3 {std::move(pt3)};

    while (f1.wait_for(100ms) != std::future_status::ready)
    {
        std::cout << ".";
        std::cout.flush();
    }

    std::cout << "\nResult f1 = " << f1.get() << std::endl;
    try
    {
        std::cout << "Result f2 = " << f2.get() << std::endl;
    }
    catch (const std::runtime_error& e)
    {
        std::cout << "Exception caught: " << e.what() << std::endl;
    }
    f3.wait();

    thd1.join();
}

class SquareCalculator
{
    std::promise<int> promise_;

public:
    void calculate(int x)
    {
        try
        {
            int result = calculate_square(x);
            promise_.set_value(result);
        }
        catch (...)
        {
            promise_.set_exception(std::current_exception());
        }
    }

    std::future<int> get_future()
    {
        return promise_.get_future();
    }
};

void futures_with_promise()
{
    SquareCalculator calc;

    std::future<int> f = calc.get_future();
    std::shared_future<int> shared_f = f.share();

    ext::joining_thread calc_thread([&calc] { calc.calculate(13); });

    ext::joining_thread process_thread([shared_f]() {
        try
        {
            int v = shared_f.get();
            std::cout << "Result: " << v << std::endl;
        }
        catch (const std::runtime_error& e)
        {
            std::cout << "caught an exception: " << e.what() << std::endl;
        }
    });

    ext::joining_thread saving_thread([shared_f]() {
        std::cout << "Saving " << shared_f.get() << " to file..." << std::endl;
    });
}

void future_with_async()
{
    std::future<void> f0 = std::async(std::launch::deferred, &save_to_file, "data.txt");

    std::this_thread::sleep_for(5s);

    f0.wait();

    auto f1 = std::async(std::launch::async, &calculate_square, 10);

    std::vector<std::future<int>> f_results;

    f_results.push_back(std::move(f1));

    for (int i = 1; i <= 10; ++i)
    {
        f_results.push_back(std::async(std::launch::async, &calculate_square, i));
    }

    for (auto& result : f_results)
    {
        try
        {
            int s = result.get();
            std::cout << s << std::endl;
        }
        catch (const std ::runtime_error& e)
        {
            std::cout << e.what() << std::endl;
        }
    }
}

template <typename Callable>
auto launch_async(Callable&& callable)
{
    using ResultT = decltype(callable());
    std::packaged_task<ResultT()> pt(std::forward<Callable>(callable));
    std::future<ResultT> f = pt.get_future();
    std::thread thd {std::move(pt)};
    thd.detach();

    return f;
}

void wtf()
{
    auto f1 = std::async(std::launch::async, &save_to_file, "one.txt");
    auto f2 = std::async(std::launch::async, &save_to_file, "two.txt");

    launch_async([] { save_to_file("three.txt"); });
    launch_async([] { save_to_file("four.txt"); });
    launch_async([] { save_to_file("five.txt"); });
}

int main()
{
    std::cout << "Main thread " << std::this_thread::get_id() << " starts..." << std::endl;

    //future_with_async();

    wtf();

    std::cout << "Main thread ends..." << std::endl;
}
