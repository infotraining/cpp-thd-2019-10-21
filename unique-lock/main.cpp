#include "joining_thread.hpp"
#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

std::timed_mutex mtx;

void background_work(size_t id, std::chrono::milliseconds timeout)
{
    std::cout << "THD#" << id << " is waiting for mutex..." << std::endl;

    std::unique_lock<std::timed_mutex> lk(mtx, std::try_to_lock);

    if (!lk.owns_lock())
    {
        do
        {
            std::cout << "THD#" << id << " doesn't own a lock..."
                      << " Tries to lock the mutex..." << std::endl;
        } while (!lk.try_lock_for(timeout));
    }

    std::cout << "Start THD#" << id << std::endl;

    std::this_thread::sleep_for(10s);

    std::cout << "End of THD#" << id << std::endl;
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    {
        ext::joining_thread thd1 {&background_work, 1, 500ms};
        ext::joining_thread thd2 {&background_work, 2, 700ms};
    } // join

    std::cout << "Main thread ends..." << std::endl;
}
