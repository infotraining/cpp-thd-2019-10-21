#include <iostream>
#include <mutex>
#include <thread>

class BankAccount
{
    const int id_;
    double balance_;
    mutable std::recursive_mutex mtx_;

public:
    BankAccount(int id, double balance)
        : id_(id)
        , balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        //        balance_ -= amount;
        //        to.balance_ += amount;

#if __cplusplus < 201703L
        std::unique_lock<std::recursive_mutex> lk_from(mtx_, std::defer_lock);
        std::unique_lock lk_to(to.mtx_, std::defer_lock); // CTAD - since C++17

        std::lock(lk_from, lk_to);
#else
        std::scoped_lock lks(mtx_, to.mtx_);
#endif

        withdraw(amount);
        to.deposit(amount);
    }

    void withdraw(double amount)
    {
        std::lock_guard lk {mtx_}; // since C++17
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<std::recursive_mutex> lk {mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<std::recursive_mutex> lk {mtx_};
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    bool try_lock()
    {
        return mtx_.try_lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

void make_withdraws(BankAccount& ba, int count, double amount)
{
    for (int i = 0; i < count; ++i)
        ba.withdraw(amount);
}

void make_deposits(BankAccount& ba, int count, double amount)
{
    for (int i = 0; i < count; ++i)
        ba.deposit(amount);
}

void make_transfers(BankAccount& from, BankAccount& to, int count, double amount)
{
    for (int i = 0; i < count; ++i)
        from.transfer(to, amount);
}

int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    std::thread thd_w {&make_withdraws, std::ref(ba1), 1'000'000, 1.0};
    std::thread thd_d {&make_deposits, std::ref(ba1), 1'000'000, 1.0};

    std::thread thd_t1 {&make_transfers, std::ref(ba1), std::ref(ba2), 10'000, 1.0};
    std::thread thd_t2 {&make_transfers, std::ref(ba2), std::ref(ba1), 10'000, 1.0};

    {
        std::scoped_lock lk {ba1, ba2};
        ba1.deposit(1'000'000.0);
        ba1.withdraw(0.99);
        ba2.withdraw(1.99);
    }

    ba1.withdraw(10.0);

    thd_d.join();
    thd_w.join();
    thd_t1.join();
    thd_t2.join();

    std::cout << "\n";

    ba1.print();
    ba2.print();
}
