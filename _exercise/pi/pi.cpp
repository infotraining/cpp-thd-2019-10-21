#include <atomic>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <future>
#include <iostream>
#include <random>
#include <thread>

using namespace std;

void calculate_counts(long no_of_attempts, long& counter)
{
    std::random_device rd;
    std::mt19937 rnd_gen(rd());
    std::uniform_real_distribution<double> rnd_distr(0.0, 1.0);

    long local_counter = 0;
    for (long n = 0; n < no_of_attempts; ++n)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);
        if (x * x + y * y < 1)
            ++local_counter;
    }

    counter += local_counter;
}

long calculate_hits(long no_of_attempts)
{
    std::random_device rd;
    std::mt19937 rnd_gen(rd());
    std::uniform_real_distribution<double> rnd_distr(0.0, 1.0);

    long local_counter = 0;
    for (long n = 0; n < no_of_attempts; ++n)
    {
        double x = rnd_distr(rnd_gen);
        double y = rnd_distr(rnd_gen);
        if (x * x + y * y < 1)
            ++local_counter;
    }

    return local_counter;
}

int main()
{
    const long N = 100'000'000;
    long counter = 0;

    cout << "Pi calc! Sync" << endl;
    auto start = chrono::high_resolution_clock::now();

    calculate_counts(N, counter);

    auto stop = chrono::high_resolution_clock::now();

    cout << "Pi = " << double(counter) / double(N) * 4 << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(stop - start).count();
    cout << " ms" << endl;

    ////////////////////////////////////////////

    cout << "Pi calc! Multi" << endl;
    start = chrono::high_resolution_clock::now();

    const size_t hardware_threads = std::max(1u, std::thread::hardware_concurrency());

    std::vector<std::thread> working_threads(hardware_threads);
    std::vector<long> results(hardware_threads);

    const auto attempts_per_thread = N / hardware_threads;

    for (size_t i = 0; i < hardware_threads; ++i)
    {
        working_threads[i] = std::thread(&calculate_counts, attempts_per_thread, std::ref(results[i]));
    }

    for (auto& thd : working_threads)
        thd.join();

    counter = std::accumulate(begin(results), end(results), 0LL);

    stop = chrono::high_resolution_clock::now();

    cout << "Pi = " << double(counter) / double(N) * 4 << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(stop - start).count();
    cout << " ms" << endl;

    ////////////////////////////////////////////

    cout << "Pi calc! Futures" << endl;
    start = chrono::high_resolution_clock::now();

    std::vector<std::future<long>> hits;

    for (size_t i = 0; i < hardware_threads; ++i)
    {
        hits.push_back(std::async(std::launch::async, &calculate_hits, attempts_per_thread));
    }

    counter = std::accumulate(begin(hits), end(hits), 0L, [](long c, auto& f) { return c + f.get(); });

    stop = chrono::high_resolution_clock::now();

    cout << "Pi = " << double(counter) / double(N) * 4 << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(stop - start).count();
    cout << " ms" << endl;

    return 0;
}
