#include "joining_thread.hpp"
#include "thread_safe_queue.hpp"
#include <cassert>
#include <chrono>
#include <functional>
#include <future>
#include <iostream>
#include <random>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

void background_work(size_t id, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    std::this_thread::sleep_for(delay);

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

int calculate_square(int x)
{
    std::cout << "Starting calculation for " << x << " in " << std::this_thread::get_id() << std::endl;

    std::random_device rd;
    std::uniform_int_distribution<> distr(100, 5000);

    std::this_thread::sleep_for(std::chrono::milliseconds(distr(rd)));

    if (x % 3 == 0)
        throw std::runtime_error("Error#3");

    return x * x;
}

void save_to_file(const std::string& filename)
{
    std::cout << "Saving to file: " << filename << std::endl;

    std::this_thread::sleep_for(3s);

    std::cout << "File saved: " << filename << std::endl;
}

using Task = std::function<void()>;

//using FollyTask = folly::Function<void()>;

class ThreadPool
{
    inline static Task end_of_work {};

    std::vector<std::thread> threads_;
    ThreadSafeQueue<Task> jobs_;

    void run()
    {
        Task job;

        while (true)
        {
            jobs_.pop(job);

            std::cout << "Popping job" << std::endl;

            if (job == nullptr)
            {
                std::cout << "Getting poisoning pill!" << std::endl;
                return;
            }

            job(); // execution of job in a pools thread
        }
    }

public:
    ThreadPool(size_t size)
        : threads_(size)
    {
        for (auto& thd : threads_)
            thd = std::thread([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    template <typename Callable>
    auto submit(Callable&& job)
    {
        using ResultT = decltype(job());

        auto pt = std::make_shared<std::packaged_task<ResultT()>>(std::forward<Callable>(job));
        auto fresult = pt->get_future();
        jobs_.push([pt] { (*pt)(); });

        //std::packaged_task<ResultT()> pt(std::forward<Callable>(job));
        //auto fresult = pt.get_future();
        //jobs_.push(std::move(pt));

        return fresult;
    }

    ~ThreadPool()
    {
        std::cout << "~ThreadPool" << std::endl;

        // sending poisining pills
        for (size_t i = 0; i < threads_.size(); ++i)
        {
            std::cout << "Sending pill" << std::endl;
            jobs_.push(end_of_work);
        }

        // waiting for finishing jobs
        for (auto& thd : threads_)
            thd.join();
    }
};

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    {
        ThreadPool thread_pool(6);

        thread_pool.submit([] { background_work(665, 2s); });
        thread_pool.submit([] { background_work(667, 3s); });

        std::vector<std::tuple<int, std::future<int>>> f_squares;
        for (int i = 1; i < 10; ++i)
        {
            f_squares.emplace_back(i, thread_pool.submit([i] { return calculate_square(i); }));
        }

        for (auto& item : f_squares)
        {
            auto x = std::get<0>(item);

            try
            {
                auto sq = std::get<1>(item).get();
                std::cout << x << " - " << sq << std::endl;
            }
            catch (const std::runtime_error& e)
            {
                std::cout << x << " - " << e.what() << std::endl;
            }
        }
    }

    std::cout << "Main thread ends..." << std::endl;
}
