#include "joining_thread.hpp"
#include <atomic>
#include <cassert>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <random>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

namespace Atomics
{
    class Data
    {
        std::vector<int> data_;
        std::atomic<bool> is_ready_ = false;

    public:
        void read()
        {
            std::cout << "Start reading..." << std::endl;
            data_.resize(100);

            std::random_device rnd;
            std::generate_n(begin(data_), 100, [&rnd] { return rnd() % 1000; });
            std::this_thread::sleep_for(2s);
            std::cout << "End reading..." << std::endl;

            is_ready_.store(true, std::memory_order_release);
        }

        void process(int id)
        {
            while (!is_ready_.load(std::memory_order_acquire))
            {
            }

            long sum = std::accumulate(begin(data_), end(data_), 0L);

            std::cout << "Id: " << id << "; Sum: " << sum << std::endl;
        }
    };
}

class Data
{
    std::vector<int> data_;
    bool is_ready_ = false;
    std::condition_variable cv_is_ready_;
    std::mutex mtx_is_ready_;

public:
    void read()
    {
        std::cout << "Start reading..." << std::endl;
        data_.resize(100);

        std::random_device rnd;
        std::generate_n(begin(data_), 100, [&rnd] { return rnd() % 1000; });
        std::this_thread::sleep_for(2s);
        std::cout << "End reading..." << std::endl;

        {
            std::lock_guard<std::mutex> lk {mtx_is_ready_};
            is_ready_ = true;
        }

        cv_is_ready_.notify_all();
    }

    void process(int id)
    {
        std::unique_lock<std::mutex> lk {mtx_is_ready_};

        //        while (!is_ready_)
        //        {
        //            cv_is_ready_.wait(lk);
        //        }

        cv_is_ready_.wait(lk, [this] { return is_ready_; });

        lk.unlock();

        long sum = std::accumulate(begin(data_), end(data_), 0L);

        std::cout << "Id: " << id << "; Sum: " << sum << std::endl;
    }
};

int main()
{
    std::cout << "Main thread"
                 " starts..."
              << std::endl;

    Data data;
    {

        ext::joining_thread thd_producer([&data] { data.read(); });

        ext::joining_thread thd_consumer1([&data] { data.process(1); });
        ext::joining_thread thd_consumer2([&data] { data.process(2); });
    }

    std::cout << "Main thread ends..." << std::endl;
}
