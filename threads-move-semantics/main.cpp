#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "joining_thread.hpp"

using namespace std::literals;

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

std::thread create_background_work()
{
    static int id = 100;

    const int current_id = ++id;
    const std::string message = "THREAD" + std::to_string(current_id);
    std::chrono::milliseconds delay(current_id);

    return std::thread {background_work, current_id, message, delay};
}

void may_throw()
{
    // throw ...
}

namespace Simplified
{
    class joining_thread
    {
        std::thread& thd_;

    public:
        joining_thread(std::thread& thd)
            : thd_ {thd}
        {
        }

        joining_thread(const joining_thread&) = delete;
        joining_thread& operator=(const joining_thread&) = delete;
        joining_thread(joining_thread&&) = delete;
        joining_thread& operator=(joining_thread&&) = delete;

        std::thread& get()
        {
            return thd_;
        }

        ~joining_thread()
        {
            if (thd_.joinable())
                thd_.join();
        }
    };
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    std::thread thd1(&background_work, 1, "THREAD1", 200ms);

    std::thread thd2 = std::move(thd1);

    std::thread thd3 = create_background_work();

    std::vector<std::thread> working_threads;
    working_threads.push_back(create_background_work());
    working_threads.push_back(create_background_work());
    working_threads.push_back(create_background_work());
    working_threads.push_back(std::move(thd2));
    working_threads.push_back(std::move(thd3));

    may_throw();

    for (auto& thd : working_threads)
        thd.join();

    ext::joining_thread jthd1(create_background_work());
    ext::joining_thread jthd2(&background_work, 1001, "Text", 100ms);

    {
        std::vector<ext::joining_thread> working_jthreads;
        working_jthreads.push_back(std::move(jthd1));
        working_jthreads.push_back(std::move(jthd2));
        working_jthreads.emplace_back(background_work, 1003, "Text", 50ms);
    } // automatic join on threads

    std::cout << "Main thread ends..." << std::endl;
}
